var app = angular.module("app", ["ngRoute", "firebase", "youtube-embed"]);
var firebaseBananaRef = new Firebase("https://dazzling-fire-5354.firebaseio.com");

app.controller("articleCtrl", ["$scope", "$routeParams", "$location", "$firebaseArray", "$firebaseObject", "$firebaseObject",
	function($scope, $routeParams, $location, $firebaseArray, $topArticle, $currentArticle) {

	$scope.articles = $firebaseArray(firebaseBananaRef.child("articles"));
	
    $scope.preview = "article-preview.html";

    $scope.goToArticle = function(id){
		$scope.currentArticle = $currentArticle(firebaseBananaRef.child("articles").child(id));
		$scope.currentArticle.$loaded().then(function() {
			if ($scope.currentArticle.updated>$scope.currentArticle.date) {
				$scope.updated = true;
			} else {
				$scope.updated = false;
			};	
		});
    	$location.path("/article/" + id);
    };

    $scope.articles.$loaded()
		.then(function() {
			if ($routeParams.id != undefined) {
				$scope.currentArticle = $currentArticle(firebaseBananaRef.child("articles").child($routeParams.id));
			}
		})
		.catch(function(error) {
			console.log("Error:", error);
	});
	
	$scope.delete = function(article) {
		if (confirm("Ta bort "+article.header+"?")) {
			$scope.articles.$remove($scope.articles.$getRecord(article.$id)).then(function() {
				$location.path("/");
			});
		};
	};
	$scope.edit = function(article) {
		$location.path("/edit-article/" + article.$id);
	};
	$scope.searchArticle = function(article) {
		return article.header.toLowerCase().match($scope.search.toLowerCase()) ? true : false;
	};	
}]);

app.controller("loginCtrl", ["$scope", "$firebaseAuth", "$location", "$firebaseArray", function($scope, $firebaseAuth, $location, $catsArray) {

	$scope.login = function() {
		firebaseBananaRef.authWithPassword({
			email    : $scope.email,
			password : $scope.password
		}, function(error, authData) {
			if (error) {
				console.log("Login Failed!", error);
				alert(error.message);
			} else {
				console.log("Authenticated successfully with payload:", authData);
			}
		});
	};

	$scope.goToRegister = function() {
		$location.path("/register/");
	};
	
	$scope.logout = function() {
		firebaseBananaRef.unauth();
	};

	$scope.auth = $firebaseAuth(firebaseBananaRef);

	$scope.auth.$onAuth(function(authData) {
      $scope.authData = authData;
      
      var adminList = ["simplelogin:1"];

      if ($scope.authData) {
      	if (adminList.indexOf($scope.authData.uid) !== -1) {
      		$scope.userStatus = "admin";
      	} else {
      		$scope.userStatus = "user";
      	}
      	$scope.name = $scope.authData.password.email.split('@')[0];
      } else {
      	$scope.userStatus = "";
      	$scope.name = "";
      }
       
    });
	console.log($scope.categories);
    $scope.search = "";
    $scope.cats = $catsArray(firebaseBananaRef.child("categories"));
    $scope.categories = "";
    $scope.catFilter = function(article) {

    	if(!$scope.categories) return true;

		if(article.categories) {
			return article.categories.match($scope.categories.name) ? true : false;

		}
		else return false;
	}
}]);

app.controller("createArticleCtrl", ["$scope", "$location", "$firebaseArray", "$firebaseArray",
	function($scope, $location, $catsArray, $artsArray) {

		$scope.createArticle = function() {
			
			var mydate = Date.now();

			console.log(mydate);

			if ($scope.imageURL === undefined) {
				$scope.imageURL = "";

			}
			if ($scope.videoURL === undefined) {
				$scope.videoURL = "";
			}
    		$scope.articles.$add({
    			header: $scope.header,
    			author: $scope.author,
				imageURL: $scope.imageURL,
    			bodyText: $scope.bodyText,
				videoURL: $scope.videoURL,
				date: mydate,
				updated: mydate,
				categories: $scope.categories.name
    		});

			$location.path("/");
    	};
	}]);

app.controller("editArticleCtrl", ["$scope", "$routeParams", "$location",
	function($scope, $routeParams, $location) {

		for (c in $scope.cats) {
			if ($scope.articles.$getRecord($routeParams.id).categories === $scope.cats[c].name) {
				$scope.categories = $scope.cats[c];
			}
		}

		$scope.header = $scope.articles.$getRecord($routeParams.id).header;
		$scope.author = $scope.articles.$getRecord($routeParams.id).author;
		$scope.bodyText = $scope.articles.$getRecord($routeParams.id).bodyText;
		$scope.videoURL = $scope.articles.$getRecord($routeParams.id).videoURL;
		$scope.imageURL = $scope.articles.$getRecord($routeParams.id).imageURL;

		$scope.saveArticle = function() {

			if ($scope.imageURL === undefined) {
				$scope.imageURL = "";

			}
			if ($scope.videoURL === undefined) {
				$scope.videoURL = "";
			}
			$scope.articles.$getRecord($routeParams.id).header = $scope.header;
			$scope.articles.$getRecord($routeParams.id).author = $scope.author;
			$scope.articles.$getRecord($routeParams.id).bodyText = $scope.bodyText;
			$scope.articles.$getRecord($routeParams.id).videoURL = $scope.videoURL;
			$scope.articles.$getRecord($routeParams.id).imageURL = $scope.imageURL;
			$scope.articles.$getRecord($routeParams.id).updated = Date.now();
			$scope.articles.$getRecord($routeParams.id).categories = $scope.categories.name;
			$scope.articles.$save($scope.articles.$getRecord($routeParams.id)).then(function() {
				$location.path("/");
			});
	    };
	}]);

app.controller("currentArticleCtrl", ["$scope", "$routeParams", "$location", "$firebaseArray",
	function($scope, $routeParams, $location, $firebaseArray) {
}]);

app.config(["$routeProvider", function($routeProvider){
	$routeProvider.when("/", {
		templateUrl: "list.html"
	});
	$routeProvider.when("/article/:id", {
		templateUrl: "article.html"
	});
	$routeProvider.when("/create-article", {
		templateUrl: "create-article.html"
	});
	$routeProvider.when("/edit-article/:id", {
		templateUrl: "edit-article.html"
	});
	$routeProvider.when("/register", {
		templateUrl: "register.html"
	});
	$routeProvider.otherwise({
		redirectTo: "/"
	});
}]);

//Controller till Kommentarer
app.controller('commentsCtrl',['$scope', '$firebaseArray', '$routeParams', function($scope, $firebaseArray, $routeParams){

	$scope.comments = $firebaseArray(firebaseBananaRef
		.child("articles")
		.child($routeParams.id)
		.child("comments"));

	$scope.submitComment = function(event){



		event.preventDefault();

		if ($scope.commentsForm.$valid) {
			$scope.comments.$add({
				name: $scope.name,
				email: $scope.authData.password.email,
				comment: $scope.comment,
				date: Date.now()
			});
			$scope.comment = "";
		} else {
			console.log("Invalid input!");
		}
	};

	$scope.deleteComment = function(comment) {
		if (confirm("Ta bort kommentaren?")) {
			$scope.comments.$remove(comment);
		};
	};
}]);


app.controller("registerCtrl", ["$scope", "$firebaseAuth", "$location", function($scope, $firebaseAuth, $location) {
	$scope.register = function(event) {

		event.preventDefault();

			if ($scope.registerForm.$valid) {
				firebaseBananaRef.createUser({
				email    : $scope.email,
				password : $scope.password
			}, function(error, userData) {
				if (error) {
			    	console.log("Error creating user:", error);
			  	} else {
			    	console.log("Successfully created user account with uid:", userData.uid);
			  	}
			  	$location.path("/");
			});
		} else {
			console.log("Invalid input!");
		}
	};
}]);
